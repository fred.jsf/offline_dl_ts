"use strict";
exports.__esModule = true;
var mongoose_1 = require("mongoose");
exports.ContextDataSchema = new mongoose_1.Schema({
    internal_id: String,
    data: mongoose_1.Schema.Types.Mixed,
    isCompressed: Boolean
});
exports.ContextData = mongoose_1.model('ContextData', exports.ContextDataSchema);
//# sourceMappingURL=contextData.js.map