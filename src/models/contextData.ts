import { Schema, Document, Model, model} from "mongoose";


export interface IContexData extends Document {
}

export const ContextDataSchema: Schema = new Schema({
    internal_id : String,
    data: Schema.Types.Mixed,
    isCompressed : Boolean
});

export const ContextData: Model<IContexData> = model<IContexData>('ContextData', ContextDataSchema);
