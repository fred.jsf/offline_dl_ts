echo 'Script Initiated'
echo 'Building application'
echo 'Removing node_modules'
rm -rf node_modules
echo 'Removing dist'
rm -rf dist
echo 'NPM INSTALL'
npm install
echo 'Install typescript'
npm install typescript -g
echo 'Build'
npm run build
echo 'Install PM2'
npm install pm2 -g
pm2 start samples/index.js
