const directline = require("../dist/bridge");
const express = require("express");

const app = express();

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.header("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS");
  next();
});

directline.initializeRoutes(app, "http://offline-dl:3000", "http://engine:3978/api/messages");

