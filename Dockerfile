# Build
FROM node:8.11.1 as builder
ENV NODE_ENV development
WORKDIR /app
COPY [".", "./"]

RUN yarn && yarn tsc

# Run
FROM node:8.11.1
ENV NODE_ENV production
WORKDIR /app
COPY --from=builder /app /app
RUN yarn

CMD node dist/